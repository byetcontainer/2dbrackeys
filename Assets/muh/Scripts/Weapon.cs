﻿using UnityEngine;
using System.Collections;


public class Weapon : MonoBehaviour {

    public float fireRate = 0f;
    public float damage = 10f;
    public LayerMask whatToHit;

    public Transform bulletTrailPrefab;
    public Transform hitPrefab;
    public Transform muzzleFlashPrefab;
    float timeToSpawnEffect = 0f;
    public float effectSpawnRate = 10f;

    //Handle camera shaking
    public float camShakeAmount = 0.1f;
    CameraShake camShake;

    float timeToFire = 0;
    Transform firePoint;


	void Awake () {
        firePoint = transform.FindChild("FirePoint");
        if (firePoint == null) {
            Debug.LogError("No firePoint could be found as a child of this object!");
        }
	}

    void Start() {
        camShake = GameMaster.gm.GetComponent<CameraShake>();
        if (camShake){
            Debug.LogError("No CameraShake script found on the GM object.");
        }
    }
	
	void Update () {
        if (fireRate == 0) {
            if (Input.GetButtonDown("Fire1")) {
                Shoot();
            }
        }
        else {
            if (Input.GetButton("Fire1") && Time.time > timeToFire) {
                timeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }

	}

    void Shoot (){
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition-firePointPosition, 200f, whatToHit);
        
        
        if (hit.collider != null) {
            Debug.DrawLine(firePointPosition, hit.point, Color.red);
            Enemy enemy = hit.collider.GetComponent<Enemy>();
            if (enemy != null) {
                enemy.DamageEnemy((int)damage);
                //Debug.Log("We hit " + hit.collider.name + " and did " + damage + " damage.");
            }
        }

        if (Time.time >= timeToSpawnEffect)
        {
            Vector3 hitPosition;
            Vector3 hitNormal;

            if (hit.collider == null){
                hitPosition = (mousePosition - firePointPosition) * 30;
                hitNormal = new Vector3(9999, 9999, 9999);
            }
            else {
                hitPosition = hit.point;
                hitNormal = hit.normal;
            }

            Effect(hitPosition, hitNormal);
            timeToSpawnEffect = Time.time + 1 / effectSpawnRate;
        }
    }

    void Effect (Vector3 hitPosition, Vector3 hitNormal) {
        Transform trail = Instantiate(bulletTrailPrefab, firePoint.position, firePoint.rotation) as Transform;
        LineRenderer lr = trail.GetComponent<LineRenderer>();

        if (lr != null) {
            lr.SetPosition(0, firePoint.position);
            lr.SetPosition(1, hitPosition);
        }

        Destroy(trail.gameObject, 0.02f);

        if(hitNormal != new Vector3(9999, 9999, 9999)){
            Transform hitParticle = Instantiate(hitPrefab, hitPosition, Quaternion.FromToRotation(Vector3.right, hitNormal)) as Transform;
            Destroy(hitParticle.gameObject, 1f);
        }

        Transform clone = Instantiate(muzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
        clone.parent = firePoint;
        float size = Random.Range(0.6f, 2.0f);
        clone.localScale = new Vector3(size, size, size);
        Destroy(clone.gameObject, 0.02f);

        //Shake the Camera
        camShake.Shake(camShakeAmount, 0.2f);
    }

 












}



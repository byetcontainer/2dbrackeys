﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Status : MonoBehaviour {

    [SerializeField]
    private RectTransform healthBarRect;
    [SerializeField]
    private Text healthText;

    void Awake() {
        if (healthBarRect == null){
            Debug.LogError("No health bar object referenced");
        }
        if (healthText == null)
        {
            Debug.LogError("No health text object referenced");
        }
    }

    public void SetHealth(int _cur, int _max) {
        float _value = (float)_cur / _max;

        healthBarRect.localScale = new Vector3(_value, 1f, 1f);
        healthText.text = _cur + "/" + _max + " HP";
    }

}



﻿using UnityEngine;
using System.Collections;


public class CameraShake : MonoBehaviour {

    public Camera mainCamera;

    float shakeAmount = 0;

    void Awake() {
        if (mainCamera == null) {
            mainCamera = Camera.main;
        }
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.T))
        {
            Shake(0.1f, 0.2f);
        }
    }

    public void Shake(float amount, float length){
        shakeAmount = amount;
        InvokeRepeating("BeginShake", 0 , 0.01f);
        Invoke("StopShake", length);
    }

    void BeginShake() {
        if (shakeAmount > 0) {
            Vector3 camPosition = mainCamera.transform.position;

            float shakeAmountX = Random.value * shakeAmount * 2 - shakeAmount;
            float shakeAmountY = Random.value * shakeAmount * 2 - shakeAmount;
            camPosition.x += shakeAmountX;
            camPosition.y += shakeAmountY;

            mainCamera.transform.position = camPosition;
        }
    }

    void StopShake() {
        CancelInvoke("BeginShake");
        mainCamera.transform.localPosition = Vector3.zero;
    }


}



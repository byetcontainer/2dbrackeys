﻿using UnityEngine;
using System.Collections;


public class WaveSpawner : MonoBehaviour {

    public enum SpawnState {
        SPAWNING, WAITING, COUNTING
    };


    //Start Wave Class
    [System.Serializable]
    public class Wave {
        public string name;     //Name of the wave
        public Transform enemy; //enemy prefab to spawn
        public int count;       //count of enemies
        public float rate;      //the rate at which enemies spawn

    }
    //End Class

    public Wave[] waves;
    private int nextWave = 0; //default nextwave

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 5f; //time in seconds
    private float waveCountdown = 0f;    //

    private float searchCountdown = 1;

    private SpawnState state = SpawnState.COUNTING;

    void Start() {
        if (spawnPoints.Length < 1)
        {
            Debug.Log("There are no spawn points to spawn to!");
            return;
        }

        waveCountdown = timeBetweenWaves;
    }

    void Update() {

        if (state == SpawnState.WAITING) {
            // Check if enemies are still alive
            if (EnemyIsAlive() == false) {
                WaveCompleted();
                return;
            }
            else {
                return;
            }
        }

        if (waveCountdown <= 0) { //if its time to start spawning waves
            if (state != SpawnState.SPAWNING) {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else {
            waveCountdown -= Time.deltaTime;
        }

    }

    bool EnemyIsAlive() {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f) {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0) {
                return false;
            }
        }


        return true;
    }

    IEnumerator SpawnWave(Wave _wave) {
        Debug.Log("Spawning Wave: " + _wave.name);
        state = SpawnState.SPAWNING;

        //Spawn things
        for (int i = 0; i < _wave.count; i++) {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = SpawnState.WAITING;

        yield break;
    }

    void WaveCompleted() {
        Debug.Log("Wave Completed!");

        state = SpawnState.COUNTING;        //reset state
        waveCountdown = timeBetweenWaves;   //reset countdown

        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            Debug.Log("ALL WAVES COMPLETE! Looping.");
        }
        else
        {
            nextWave++;
        }

    }

    void SpawnEnemy(Transform _enemy) {
        //Spawn enemy
        Debug.Log("Spawning Enemy: " + _enemy.name);
    
        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_enemy, transform.position, transform.rotation);
   } 

}


